#ifndef _JSON_H
#define _JSON_H

#include <stdbool.h>

/**
 * Structure: print_type_t
 * Typedef: print_type_t
 * Author: Joachim M. Giæver
 * 
 * Sections:
 *	JSON_SAVE_FILE,
 *	JSON_PRINT,
 *	JSON_PPRINT,
 *	JSON_TYPE
 *
 * Description:
 *  Representation of JSON after processing.
 **/
typedef enum {
	JSON_SAVE_FILE = 0x32f,
	JSON_PRINT = JSON_SAVE_FILE << 1,
	JSON_PPRINT = JSON_PRINT << 1,
	JSON_TYPE
} print_type_t;

/**
 * Enum: json_type_t
 * Typedef: json_type_t
 * Author: Joachim M. Giæver
 * 
 * Sections:
 * - describes itself
 *
 * Description:
 * Defines entries in JSON
 **/
typedef enum {
	__JSON__	= 0x1,
	JSON_OBJ 	= __JSON__ << 1,
	JSON_MEM 	= JSON_OBJ << 1,
	JSON_ELEM 	= JSON_MEM << 1,
	JSON_PAIR 	= JSON_ELEM << 1,
	JSON_ARR 	= JSON_PAIR << 1,
	JSON_VAL 	= JSON_ARR << 1,
	JSON_CHAR 	= JSON_VAL << 1,
	JSON_INT 	= JSON_CHAR << 1,
	JSON_FLOAT 	= JSON_INT << 1,
	JSON_BOOL 	= JSON_FLOAT << 1,
	JSON_NULL 	= JSON_BOOL << 1,
	JSON_TYPE_NOTSET
} json_types_t;

/**
 * Structure: json_type
 * Typedef: json_type_t
 * Author: Joachim M. Giæver
 * 
 * Sections:
 *	json_types_t type: 		Type of an JSON-entry.
 *
 * Description:
 * 	Holds the type of an JSON-entry.
 **/
typedef struct json_type json_type_t;

/**
 * JSON-entry types
 **/

/**
 * Structure: 
 * Typedef: 
 * Author: Joachim M. Giæver
 * 
 * Sections:
 *	json_types_t type:		Entry type (MUST BE FIRST)
 *	json_mem_t *members:	JSON-meners entries.
 *	print_type_t ptype:		Print type, eg Pretty print.
 *	int code:				Status code.
 *
 * Description:
 * 	Defines an JSON-object.
 **/
typedef struct json_obj json_obj_t;

/**
 * Structure: json_mem
 * Typedef: json_mem_t
 * Author: Joachim M. Giæver
 * 
 * Sections:
 *	json_types_t type:		Entry type (MUST BE FIRST)
 *	void *entry:			Containing entry. Either pair or new member
 *	void *next:				Simblings; other members or pairs in parent.
 *
 * Description:
 * 	Defines an JSON-member entry
 **/
typedef struct json_mem json_mem_t;

/**
 * Structure: json_elem
 * Typedef: json_elem_t
 * Author: Joachim M. Giæver
 * 
 * Sections:
 *	json_types_t type:		Entry type (MUST BE FIRST).
 *	void *entry:			Containing entry. Either value or new element.
 *	void *next:				Simblings; other elements in parent.
 *
 * Description:
 *  Defines an JSON-element entry.
 **/
typedef struct json_elem json_elem_t;

/**
 * Structure: json_pair
 * Typedef: json_pair_t
 * Author: Joachim M. Giæver
 * 
 * Sections:
 *	json_types_t type:		Entry type (MUST BE FIRST).
 *	char *str:				String specifier.
 *	json_val_t *value;		Containing entry, only value.
 *	void *next:				Simblings; other members or pairs in parent.
 *
 * Description:
 * 
 **/
typedef struct json_pair json_pair_t;

/**
 * Structure: json_arr
 * Typedef: json_arr_t
 * Author: Joachim M. Giæver
 * 
 * Sections:
 *	json_types_t type: 		Entry type (MUST BE FIRST).
 *	void *entry:			Containing entry, only elements.
 *
 * Description:
 *  Defines a JSON-array entry.
 **/
typedef struct json_arr json_arr_t;

/**
 * Structure: json_val
 * Typedef: json_val_t
 * Author: Joachim M. Giæver
 * 
 * Sections:
 *	json_types_t type:		Entry type (MUST BE FIRST).
 * 	json_types_t sub_type:	Type of the sub entry, eg JSON_INT, _CHAR etc
 * 	void *value:			The actual entry
 *
 * Description:
 * 
 **/
typedef struct json_val json_val_t;

typedef void json_printf_t (void *, ...);

/**
 * Result codes; OK = 0 =< resultcode > 0 = Failure
 **/
#define JSON_INS_ELEM_ARR 		11
#define JSON_INS_VAL_ELEM		9
#define JSON_INS_VAL_VAL		8
#define JSON_INS_TYPE_VAL		7
#define JSON_INS_VAL_PAIR		6
#define JSON_INS_PAIR_MEM		5
#define JSON_INS_MEM_OBJ		3
#define JSON_INS_CORRECTLY		2
#define JSON_OK 				1
#define JSON_NO_REPORT			0
#define JSON_UNKNOWN_PTYPE		-1
#define JSON_UNINITALIZED_OJB	-2
#define JSON_NOMEM 				-3
#define JSON_INVALID_ROOT_ENT	-4
#define JSON_INVALID_OBJ_ENT	-5
#define JSON_INVALID_MEM_ENT	-6
#define JSON_INVALID_PAIR_ENT	-7
#define JSON_INVALID_ELE_MENT	-8
#define JSON_INVALID_VAL_TYPE	-9
#define JSON_INVALID_VAL_ENT	-10
#define JSON_INVALID_ELEM_ENT 	-11
#define JSON_INVALID_ARR_ENT	-12
#define JSON_MIXED_VAL_AND_TYPE	-13
#define JSON_INVALID_STRUCTURE	-100

/**
 * Function: json_create
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - char *fname:			The filename; on saving
 							and possible parsing - in the future.
 * - print_type_t ptype:	Store/print type
 *
 * Description:
 * Creates the main root of the JSON-structure.
 *
 * Returns: json_obj_t *, the mother root
 **/
json_obj_t *json_create( char *fname, print_type_t ptype );

/**
 * Function: json_destroy
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_obj_t *jobj:		JSON-object to destroy
 *
 * Description:
 * Destroys any kind of JSON-objects
 *
 * Returns: void
 **/
void json_destroy( json_obj_t *jobj );

/**
 * Function: json_create_obj
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_obj_t *jobj:		JSON-object which is the main-root
 * - json_val_t *jval:		JSON-value to insert to
 *
 * Description:
 * Creates a new JSON-object to insert into the 
 * JSON-structure (only into JSON-value.).
 *
 * Works recursivly on children.
 *
 * Returns: json_obj_t *, JSON-object
 **/
json_obj_t *json_create_obj( json_obj_t *jobj, json_val_t *jval );

/**
 * Function: json_create_mem
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_obj_t *jobj:		JSON-object to insert to
 *
 * Description:
 * Creates a new JSON-member and inserts it into the 
 * JSON-object. May be any kind of JSON-object, both
 * root or children.
 *
 * Returns: json_mem_t *, JSON-member
 **/
json_mem_t *json_create_mem( json_obj_t *jobj );

/**
 * Function: json_destroy_mem
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_mem_t *jmem:		JSON-member to destroy
 *
 * Description:
 * Destroy a JSON-member, works recursively on both simblings
 * and children.
 *
 * Returns: void
 **/
void json_destroy_mem( json_mem_t *jmem );

/**
 * Function: json_create_pair
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_obj_t *jobj:		JSON-object which is the main-root
 * - json_mem_t *jmem:		JSON-member to insert to
 *
 * Description:
 * Creates a new JSON-pair and inserts it into a JSON-member.
 *
 * Returns: json_pair_t *, JSON-pair
 **/
json_pair_t *json_create_pair( json_obj_t *jobj, json_mem_t *jmem, char *str );

/**
 * Function: json_destroy_pair
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_pair_t *jpair:	JSON-pair to destroy
 *
 * Description:
 * Destroys a JSON-pair. Works recursively on both the simblings
 * and children.
 *
 * Returns: void
 **/	
void json_destroy_pair( json_pair_t *jpair );

/**
 * Function: json_create_arr
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_obj_t *jobj:		JSON-object which is the main-root
 * - json_val_t *jval:		JSON-value to insert to
 *
 * Description:
 * Creates a new JSON-array and inserts it into JSON-value.
 *
 * Returns: json_arr_t *, JSON-array
 **/
json_arr_t *json_create_arr( json_obj_t *jobj, json_val_t *jval );

/**
 * Function: json_destroy_arr
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_arr_t *jarr: 		JSON-array to destroy
 *
 * Description:
 * Destroys a JSON-array. Works recursively on the children.
 *
 * Returns: void
 **/
void json_destroy_arr( json_arr_t *jarr );

/**
 * Function: json_create_elem
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_obj_t *jobj:		JSON-object which is the main-root
 * - json_arr_t *jarr: 		JSON-array to insert into
 *
 * Description:
 * Creates a JSON-element and inserts it int an JSON-array.
 *
 * Returns: json_elem_t *, JSON-element
 **/
json_elem_t *json_create_elem( json_obj_t *jobj, json_arr_t *jarr );

/**
 * Function: json_destroy_elem
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_elem_t *jelem:	JSON-element to destroy
 *
 * Description:
 * Destroys a JSON-element. Works recursively on both simblings
 * and children.
 *
 * Returns: void
 **/
void json_destroy_elem( json_elem_t *jelem );

/**
 * Function: json_insert_value
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_obj_t *jobj:		JSON-object which is the main-root
 * - void *ins_obj:			Object or element to insert into
 * - json_types_t vtype:	Type of the held element in value
 * - void *entry:			Entry to store in value
 *
 * Description:
 * Creates a new JSON-value and inserts it into a JSON-element or
 * a JSON-pair. The value must also be specified. JSON-object and 
 * JSON-array can be insert upon creation by setting entry as NULL.
 * 
 * Ex:
 *
 * // Insert this value into a JSON-pair, will contain an JSON-array
 * json_val_t *jvalue = json_insert_value( jobj, jpair, JSON_ARR, NULL );
 *
 * // Create the JSON-array and insert it into the above value
 * json_arr_t *jarr = json_create_arr( jobj, jval );
 *
 * Returns: json_val_t *, JSON-value
 **/
json_val_t *json_insert_value( json_obj_t *jobj, void *ins_obj, json_types_t vtype, void *entry );

/**
 * Function: json_destroy_value
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_val_t *jval:		JSON-value to destroy
 *
 * Description:
 * Destroys a JSON-value. Works recursively on children.
 *
 * Returns: void
 **/
void json_destroy_value( json_val_t *jval );

/**
 * Function: json_has_err
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_obj_t *jobj:		JSON-object which is the main-root
 *
 * Description:
 * Returns whether or not there was an error present on last event.
 *
 * Returns: int 0 on non errors 1 on errors
 **/
int json_has_err( json_obj_t *jobj );

/**
 * Function: json_last_err
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_obj_t *jobj:		JSON-object which is the main-root
 *
 * Description:
 * Returns the last error code present.
 *
 * Returns: int, >= 0 on non errors <= 0 on errors
 **/
int json_last_err( json_obj_t *jobj );

/**
 * Function: json_last_errmsg
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_obj_t *jobj:		JSON-object which is the main-root
 *
 * Description:
 * Returns the error message related to the error code
 * stored in the JSON-object (remember to check for errors; json_has_err).
 *
 * Returns: char *, error message
 **/
const char *json_last_errmsg( json_obj_t *jobj );

/**
 * Function: 
 * Author: Joachim M. Giæver
 * 
 * Parameters:
 * - json_obj_t *jobj:		JSON-object which is the main-root
 *
 * Description:
 * Recursively prints the JSON-object.
 *
 * Returns: void
 **/
void json_print( json_obj_t *jobj );

#endif