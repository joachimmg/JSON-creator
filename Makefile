# MAKEFILE

CC=gcc
CCOPTS=-Wall -g
CCL=-lpthread -lm 

JSON_SRC=json.c
JSON_HEADERS=json.h

COMMON_SRC=common.c
COMMON_HEADER=common.h

all: json-test

json-test: $(JSON_SRC) $(COMMON_SRC) $(JSON_HEADERS) $(COMMON_HEADER) Makefile
	$(CC) $(CCOPTS) $(JSON_SRC) $(COMMON_SRC) -o $@ -D_DEBUG_JSON=1 -include $(JSON_HEADERS) $(COMMON_HEADER) $(CCL)

clean:
	rm -f *~ *.o *.exe *.stackdump sql-* json-* *.dot* *.txt