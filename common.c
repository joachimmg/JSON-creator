#include <stdio.h>
#include <stdlib.h>
#include <stdarg.h>

void errandend( const char *format, ... ) {

	va_list vargs;

	va_start( vargs, format );
	
	printf( " | " );
    printf( format, vargs );
    
    va_end( vargs );

    exit( EXIT_FAILURE );
}